/****************************************************************************
http://retro.moe/unijoysticle2

Copyright 2019 Ricardo Quesada

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
****************************************************************************/

#ifndef UNI_GBA_H
#define UNI_GBA_H

#include <stdint.h>

#include "uni_gamepad.h"

// Valid for Amiga, Atari 8-bit, Atari St, C64 and others...
typedef struct {
  uint8_t up;
  uint8_t down;
  uint8_t left;
  uint8_t right;
  uint8_t a;
  uint8_t b;
  uint8_t l;
  uint8_t r;
  uint8_t start;
  uint8_t select;
} uni_gba_t;

void to_gba(const uni_gamepad_t* gp, uni_gba_t* out_joy);

#endif  // UNI_JOYSTICK_H
