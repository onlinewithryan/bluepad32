
/****************************************************************************
http://retro.moe/unijoysticle2

Copyright 2019 Ricardo Quesada

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
****************************************************************************/

#include "uni_gba.h"

void to_gba(const uni_gamepad_t* gp, uni_gba_t* out_joy) {
  if (gp->updated_states & GAMEPAD_STATE_BUTTON_A) {
    out_joy->a |= ((gp->buttons & BUTTON_A) != 0);
  }
  if (gp->updated_states & GAMEPAD_STATE_BUTTON_B) {
    out_joy->b |= ((gp->buttons & BUTTON_B) != 0);
  }

  if (gp->updated_states & GAMEPAD_STATE_BUTTON_SHOULDER_R) {
    out_joy->r |= ((gp->buttons & BUTTON_SHOULDER_R) != 0);
  }

  if (gp->updated_states & GAMEPAD_STATE_BUTTON_SHOULDER_L) {
    out_joy->l |= ((gp->buttons & BUTTON_SHOULDER_L) != 0);
  }

  if (gp->updated_states & GAMEPAD_STATE_MISC_BUTTON_HOME) {
    out_joy->start |= ((gp->misc_buttons & MISC_BUTTON_HOME) != 0);
  }

  if (gp->updated_states & GAMEPAD_STATE_MISC_BUTTON_BACK) {
    out_joy->select |= ((gp->misc_buttons & MISC_BUTTON_BACK) != 0);
  }

  // Dpad
  if (gp->updated_states & GAMEPAD_STATE_DPAD) {
    if (gp->dpad & 0x01) out_joy->up |= 1;
    if (gp->dpad & 0x02) out_joy->down |= 1;
    if (gp->dpad & 0x04) out_joy->right |= 1;
    if (gp->dpad & 0x08) out_joy->left |= 1;
  }

  // Axis: X and Y
  if (gp->updated_states & GAMEPAD_STATE_AXIS_X) {
    out_joy->left |= (gp->axis_x < -AXIS_THRESHOLD);
    out_joy->right |= (gp->axis_x > AXIS_THRESHOLD);
  }
  if (gp->updated_states & GAMEPAD_STATE_AXIS_Y) {
    out_joy->up |= (gp->axis_y < -AXIS_THRESHOLD);
    out_joy->down |= (gp->axis_y > AXIS_THRESHOLD);
  }
}
