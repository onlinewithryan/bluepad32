/****************************************************************************
http://retro.moe/gba2

Copyright 2019 Ricardo Quesada

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
****************************************************************************/

// Unijoysticle platform

#include "uni_platform_gba.h"

#include <driver/gpio.h>
#include <esp_timer.h>
#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <freertos/queue.h>
#include <math.h>

#include "rom/rtc.h"
#include "soc/rtc.h"
#include "soc/rtc_cntl_reg.h"
#include "uni_bluetooth.h"
#include "uni_config.h"
#include "uni_debug.h"
#include "uni_gamepad.h"
#include "uni_gba.h"
#include "uni_hid_device.h"
// --- Consts

/*
If you use GPIO pins between 0 and 31 (they do NOT have to be sequential)
parallel writing will be possible. Writing in parallel give extremely reliable
performance in games with code for multiple button pushes. i.e. in Link's
Awakening, pressing START + SELECT + A + B at the same time will trigger the
save screen. When PLAT_GBA_ENABLE_PARALLEL_WRITE is 0 this button combo is very
hard to pull off. You need to get lucky with the ESP32 writing clock and the GBA
input reading clock. When PLAT_GBA_ENABLE_PARALLEL_WRITE is 1 this becomes
trivial to pull off. If you use GPIO larger than 31, GPIO_OUT_REG will not be
able to cover those pins in one write.

NOTE: only GPIO pins >= 12 should actually be used, 0-11 is used by the system.
NOTE: BOOT will fail if GPIO 12 is pulled high during boot, so we should not use
GPIO 12 as an output
NOTE: GPIO 14 and 15 output a PWM signal at boot so they should also not be used
*/

/*
 These are the physical GBA TP (test point) to GPIO mappings. We have two sets
 of enums here one for the physical mapping and one for the logical mapping to
 allow us to easily remap buttons in the code.
 TODO: add in button remapping
 */
enum {
  GBA_TP_6_UP = GPIO_NUM_13,
  GBA_TP_7_DOWN = GPIO_NUM_16,
  GBA_TP_5_LEFT = GPIO_NUM_17,
  GBA_TP_4_RIGHT = GPIO_NUM_18,
  GBA_TP_0_A = GPIO_NUM_19,
  GBA_TP_1_B = GPIO_NUM_21,
  GBA_TP_9_L = GPIO_NUM_22,
  GBA_TP_8_R = GPIO_NUM_23,
  GBA_TP_3_START = GPIO_NUM_25,
  GBA_TP_2_SELECT = GPIO_NUM_26,
};

// Use this enum to remap any controller keys in code.
// Most controllers have a button layput like A, B
// The GBA has a button layout B, A so we swap them bellow.
enum {
  GPIO_UP = GBA_TP_6_UP,
  GPIO_DOWN = GBA_TP_7_DOWN,
  GPIO_LEFT = GBA_TP_5_LEFT,
  GPIO_RIGHT = GBA_TP_4_RIGHT,
  GPIO_A = GBA_TP_1_B,  // A and B swapped for 8bitdo
  GPIO_B = GBA_TP_0_A,
  GPIO_L = GBA_TP_9_L,
  GPIO_R = GBA_TP_8_R,
  GPIO_START = GBA_TP_3_START,
  GPIO_SELECT = GBA_TP_2_SELECT,
};

#define PLAT_GBA_ENABLE_PARALLEL_WRITE 1
// Some controllers will spam the unpressed state.
// To save energy we can check if the state matches the new incoming one
#define PLAT_GBA_IGNORE_DUPLICATE_WRITE 1

typedef struct gba_instance_s {
  uni_gamepad_seat_t gamepad_seat;       // which "seat" (port) is being used
  uni_gamepad_seat_t prev_gamepad_seat;  // which "seat" (port) was used before
} gba_instance_t;
_Static_assert(sizeof(gba_instance_t) < HID_DEVICE_MAX_PLATFORM_DATA,
               "Unijoysticle intance too big");

// --- Constants
static const gpio_num_t JOY_A_PORTS[] = {
    GPIO_UP, GPIO_DOWN, GPIO_LEFT, GPIO_RIGHT, GPIO_A,
    GPIO_B,  GPIO_L,    GPIO_R,    GPIO_START, GPIO_SELECT,
};
static const bd_addr_t zero_addr = {0, 0, 0, 0, 0, 0};
// --- Code

static gba_instance_t* get_gba_instance(uni_hid_device_t* d);
static void set_gamepad_seat(uni_hid_device_t* d, uni_gamepad_seat_t seat);
static void process_gba(uni_gba_t* joy, uni_gamepad_seat_t seat);
static const int32_t USED_GPIO_PINS_BITMASK =
    ((1 << GPIO_UP) | (1 << GPIO_DOWN) | (1 << GPIO_LEFT) | (1 << GPIO_RIGHT) |
     (1 << GPIO_A) | (1 << GPIO_B) | (1 << GPIO_L) | (1 << GPIO_R) |
     (1 << GPIO_START) | (1 << GPIO_SELECT));

int32_t last_gpio_bitmap = USED_GPIO_PINS_BITMASK;

// TODO: are these needed? copied from uni_platform_unijoysticle
#define MAX(a, b)           \
  ({                        \
    __typeof__(a) _a = (a); \
    __typeof__(b) _b = (b); \
    _a > _b ? _a : _b;      \
  })

#define MIN(a, b)           \
  ({                        \
    __typeof__(a) _a = (a); \
    __typeof__(b) _b = (b); \
    _a < _b ? _a : _b;      \
  })

//
// Platform Overrides
//
static void gba_init(int argc, const char** argv) {
  UNUSED(argc);
  UNUSED(argv);

  gpio_config_t io_conf;
  io_conf.intr_type = GPIO_INTR_DISABLE;
  /*
   GPIO_MODE_INPUT_OUTPUT is important here.
   The GBA pads supply 3.3v and to emulate a button press
   you need to pull that value down to 0v.
   Setting GPIO_MODE_INPUT_OUTPUT allows the ESP32 to set a high state
   without pushing voltage back to the GBA.
  */
  io_conf.mode = GPIO_MODE_INPUT_OUTPUT;
  io_conf.pull_down_en = GPIO_PULLDOWN_ENABLE;
  // Disable pullup so we don't push voltage to the GBA
  io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
  io_conf.pin_bit_mask = USED_GPIO_PINS_BITMASK;

  ESP_ERROR_CHECK(gpio_config(&io_conf));

  // Set high all GPIOs to simulate no buttons being pushed.
  const int MAX_GPIOS = sizeof(JOY_A_PORTS) / sizeof(JOY_A_PORTS[0]);
  for (int i = 0; i < MAX_GPIOS; i++) {
    ESP_ERROR_CHECK(gpio_set_level(JOY_A_PORTS[i], 1));
  }

  // TODO setup a dedicated sync button
}

static uint32_t getCpuFrequencyMhz() {
  rtc_cpu_freq_config_t conf;
  rtc_clk_cpu_freq_get_config(&conf);
  return conf.freq_mhz;
}

static void gba_on_init_complete(void) {
  loge("DEBUG: gba_on_init_complete called\n");
  loge("DEBUG: getCpuFrequencyMhz %d\n", getCpuFrequencyMhz());
  loge("DEBUG: getXtalFrequencyMhz %d\n", rtc_clk_xtal_freq_get());
  // TODO: find out how to lower the clock outside of the config
  // TODO: turn off wifi to use less power
}

static void gba_on_device_connected(uni_hid_device_t* d) {
  if (d == NULL) {
    loge("ERROR: gba_on_device_connected: Invalid NULL device\n");
  }
}

static void gba_on_device_disconnected(uni_hid_device_t* d) {
  enable_bluetooth_scanning();
  if (d == NULL) {
    loge("ERROR: gba_on_device_disconnected: Invalid NULL device\n");
    return;
  }
  gba_instance_t* ins = get_gba_instance(d);

  if (ins->gamepad_seat != GAMEPAD_SEAT_NONE) {
    ins->gamepad_seat = GAMEPAD_SEAT_NONE;
  }
}

static int gba_on_device_ready(uni_hid_device_t* d) {
  if (d == NULL) {
    loge("ERROR: gba_on_device_ready: Invalid NULL device\n");
    return -1;
  }
  gba_instance_t* ins = get_gba_instance(d);

  // Some safety checks. These conditions should not happen
  if ((ins->gamepad_seat != GAMEPAD_SEAT_NONE) ||
      (!uni_hid_device_has_controller_type(d))) {
    loge("ERROR: gba_on_device_ready: pre-condition not met\n");
    return -1;
  }
  uint32_t used_joystick_ports = 0;
  for (int i = 0; i < UNI_HID_DEVICE_MAX_DEVICES; i++) {
    uni_hid_device_t* tmp_d = uni_hid_device_get_instance_for_idx(i);
    used_joystick_ports |= get_gba_instance(tmp_d)->gamepad_seat;
  }
  int wanted_seat = GAMEPAD_SEAT_A;
  set_gamepad_seat(d, wanted_seat);
  return 0;
}

#if !PLAT_GBA_ENABLE_PARALLEL_WRITE
// This is only really needed if you use gpio pins > 31
static void joy_update_gba_serial(uni_gba_t* joy) {
  gpio_set_level(GPIO_UP, !joy->up);
  gpio_set_level(GPIO_DOWN, !joy->down);
  gpio_set_level(GPIO_LEFT, !joy->left);
  gpio_set_level(GPIO_RIGHT, !joy->right);
  gpio_set_level(GPIO_A, !joy->a);
  gpio_set_level(GPIO_B, !joy->b);
  gpio_set_level(GPIO_L, !joy->l);
  gpio_set_level(GPIO_R, !joy->r);
  gpio_set_level(GPIO_START, !joy->start);
  gpio_set_level(GPIO_SELECT, !joy->select);
}

#else  // PLAT_GBA_ENABLE_PARALLEL_WRITE == 1
static int32_t joy_to_bitmap(uni_gba_t* joy) {
  /*
  We need to set the gpio level to the inverse of the button push since,
  high voltage means not pushed and pulling to ground means pushed.
  */
  return (0 | (!joy->up << GPIO_UP) | (!joy->down << GPIO_DOWN) |
          (!joy->left << GPIO_LEFT) | (!joy->right << GPIO_RIGHT) |
          (!joy->a << GPIO_A) | (!joy->b << GPIO_B) | (!joy->l << GPIO_L) |
          (!joy->r << GPIO_R) | (!joy->start << GPIO_START) |
          (!joy->select << GPIO_SELECT));
}

static void joy_update_gba_parallel(uni_gba_t* joy) {
  /*
  Some controllers will use interupts to only call this function when the
  button state has changed others will poll on an interval. It is expensive to
  constantly change the gpio values if there is no new state. This check will
  break early if we notice nothing is different than the current state of the
  gpios.
  */
  int32_t current_joy_bitmap = joy_to_bitmap(joy);
  if (current_joy_bitmap == last_gpio_bitmap) {
    return;
  }
  last_gpio_bitmap = current_joy_bitmap;
  // Read state of GPIO pins 0-31, set only our controller pins to 0
  // then or with the actual joypad values we want to set.
  uint32_t output =
      (REG_READ(GPIO_OUT_REG) & ~USED_GPIO_PINS_BITMASK) | current_joy_bitmap;

  REG_WRITE(GPIO_OUT_REG, output);
}
#endif

static void joy_update_gba(uni_gba_t* joy) {
  logi(
      "up=%d, down=%d, left=%d, right=%d, A=%d, B=%d, L=%d, R=%d, START=%d, "
      "SELECT=%d\n",
      joy->up, joy->down, joy->left, joy->right, joy->a, joy->b, joy->l, joy->r,
      joy->start, joy->select);

  /*
  We need to set the gpio level to the inverse of the button push since,
  high voltage means not pushed and pulling to ground means pushed.
  */
#if PLAT_GBA_ENABLE_PARALLEL_WRITE
  joy_update_gba_parallel(joy);
#else   // PLAT_GBA_ENABLE_PARALLEL_WRITE == 0
  joy_update_gba_serial(joy);
#endif  // PLAT_GBA_ENABLE_PARALLEL_WRITE
}

static void gba_on_gamepad_data(uni_hid_device_t* d, uni_gamepad_t* gp) {
  if (d == NULL) {
    loge("ERROR: gba_on_device_gamepad_data: Invalid NULL device\n");
    return;
  }

  gba_instance_t* ins = get_gba_instance(d);

  // FIXME: Add support for EMULATION_MODE_COMBO_JOY_MOUSE
  uni_gba_t joy, joy_ext;
  memset(&joy, 0, sizeof(joy));
  memset(&joy_ext, 0, sizeof(joy_ext));
  to_gba(gp, &joy);
  process_gba(&joy, ins->gamepad_seat);
}

static int32_t gba_get_property(uni_platform_property_t key) { return 0; }

static void gba_on_device_oob_event(uni_hid_device_t* d,
                                    uni_platform_oob_event_t event) {
  if (d == NULL) {
    loge("ERROR: gba_on_device_gamepad_event: Invalid NULL device\n");
    return;
  }

  if (event != UNI_PLATFORM_OOB_GAMEPAD_SYSTEM_BUTTON) {
    loge("ERROR: gba_on_device_oob_event: unsupported event: 0x%04x\n", event);
    return;
  }

  gba_instance_t* ins = get_gba_instance(d);

  if (ins->gamepad_seat == GAMEPAD_SEAT_NONE) {
    loge(
        "gba: cannot swap port since device has joystick_port = "
        "GAMEPAD_SEAT_NONE\n");
    return;
  }
  return;
}

//
// Helpers
//

static gba_instance_t* get_gba_instance(uni_hid_device_t* d) {
  return (gba_instance_t*)&d->platform_data[0];
}

static void process_gba(uni_gba_t* joy, uni_gamepad_seat_t seat) {
  if (seat == GAMEPAD_SEAT_A) {
    joy_update_gba(joy);
  } else {
    logd("gba: process_gba: invalid gamepad seat: %d\n", seat);
  }
}

static void set_gamepad_seat(uni_hid_device_t* d, uni_gamepad_seat_t seat) {
  gba_instance_t* ins = get_gba_instance(d);
  ins->gamepad_seat = seat;

  logd("gba: device %s has new gamepad seat: %d\n", bd_addr_to_str(d->address),
       seat);
  disable_bluetooth_scanning();

  // Fetch all enabled ports
  uni_gamepad_seat_t all_seats = GAMEPAD_SEAT_NONE;
  for (int i = 0; i < UNI_HID_DEVICE_MAX_DEVICES; i++) {
    uni_hid_device_t* tmp_d = uni_hid_device_get_instance_for_idx(i);
    if (tmp_d == NULL) continue;
    if (bd_addr_cmp(tmp_d->address, zero_addr) != 0) {
      all_seats |= get_gba_instance(tmp_d)->gamepad_seat;
    }
  }

  if (d->report_parser.set_lightbar_color != NULL) {
    // First try with color LED (best experience)
    uint8_t red = 0;
    uint8_t green = 0;
    if (seat & 0x01) green = 0xff;
    if (seat & 0x02) red = 0xff;
    d->report_parser.set_lightbar_color(d, red, green, 0x00 /* blue*/);
  } else if (d->report_parser.set_player_leds != NULL) {
    // 2nd best option: set player LEDs
    d->report_parser.set_player_leds(d, all_seats);
  } else if (d->report_parser.set_rumble != NULL) {
    // This was causing a non stop rumble on 8Bitdo N30 Pro 2
    // d->report_parser.set_rumble(d, 0x80 /* value */, 0x04 /* duration */);
  }
}

//
// Entry Point
//
struct uni_platform* uni_platform_gba_create(void) {
  static struct uni_platform plat;

  plat.name = "gba";
  plat.init = gba_init;
  plat.on_init_complete = gba_on_init_complete;
  plat.on_device_connected = gba_on_device_connected;
  plat.on_device_disconnected = gba_on_device_disconnected;
  plat.on_device_ready = gba_on_device_ready;
  plat.on_device_oob_event = gba_on_device_oob_event;
  plat.on_gamepad_data = gba_on_gamepad_data;
  plat.get_property = gba_get_property;

  return &plat;
}
